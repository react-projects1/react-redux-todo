import React, { Component } from 'react';
import {connect} from 'react-redux';
import {addtodo} from '../redux/todo/actions';

class AddTodo extends Component {
    constructor(props){
        super(props);
        this.state = {
            input:""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    handleChange(e) {
        this.setState({input:e.target.value})
    }
    handleClick(){
        this.props.addtodo(this.state.input);
        this.setState({input:""})
    }
    render() {
        return (
            <div>
                <input type="text" value={this.state.input} onChange={this.handleChange}/>
                <button onClick={this.handleClick}>Add</button>
            </div> 
        );
    }
}
export default connect(null,{addtodo})(AddTodo)