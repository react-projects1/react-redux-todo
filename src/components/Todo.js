import * as React from 'react';
import {connect} from 'react-redux';
import { toggletodo } from '../redux/todo/actions';

function Todo(props) {
    return (
    <li onClick= {() => props.toggletodo(props.t.id)}>
        {props.t && props.t.complete ? <span style={{color:'green'}}>Done</span>:<span style={{color:'red'}}>ToDo</span>}
        -{props.t.content}
    </li>
    );
};

const mapDispathToProps = dispath => {
    return{
        toggletodo : id => dispath(toggletodo(id))
    }
}

export default connect(null,mapDispathToProps)(Todo);