import * as React from 'react';
import {connect }from 'react-redux';
import Todo from './Todo';

function TodoList(props) {
    return (
        <div>
            <ul>
                {props.todos && props.todos.length ?
                 props.todos.map((t , i) =>{
                    return <Todo t={t} key={i}/>
             }):""}
            </ul> 
        </div>
    );
};
const mapStateToProps = state => {
    return {
        todos:state.data
    }
}

export default connect(mapStateToProps)(TodoList);