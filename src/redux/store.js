import {createStore} from 'redux';
import reducer from './todo/reducers';

export default createStore(reducer);