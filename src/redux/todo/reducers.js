import { ADD_TODO , TOGGLE_TODO} from './types';
import { act } from 'react-dom/test-utils';

const initialstate = {
    data:[]
}
const reducer = (state=initialstate , action) => {
    switch(action.type) {
        case ADD_TODO :
            return{
                ...state,
                data:[
                    ...state.data,
                    {
                        id:action.payload.id,
                        content:action.payload.content,
                        complete:false
                    }
                ]
            }
        case TOGGLE_TODO:
            return{
                ...state , 
                  data:state.data.map(d => {
                    if(d.id === action.payload.id) {
                        return{
                            id:d.id,
                            content:d.content,
                            complete:!d.complete
                        }
                    }else {
                        return d
                    }
                  })
                }

        default : return state   
    }
}
export default reducer