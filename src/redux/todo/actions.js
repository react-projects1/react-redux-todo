import { ADD_TODO , TOGGLE_TODO} from './types';

let initialId = 0;

export const addtodo = text => {
    return{
        type: ADD_TODO,
        payload:{
            id:++initialId ,
            content:text
        }

    }
}

export const toggletodo =id => {
    return{
        type:TOGGLE_TODO,
        payload:{
            id
        }
    }
}
